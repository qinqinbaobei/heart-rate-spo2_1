from max30102 import MAX30102
from machine import SoftI2C, Pin
import utime

my_SDA_pin = 10
my_SCL_pin = 6
my_i2c_freq = 400000

i2c = SoftI2C(sda=Pin(my_SDA_pin),
              scl=Pin(my_SCL_pin),
              freq=my_i2c_freq)
i2c.scan()

sensor = MAX30102(i2c=i2c)
sensor.setup_sensor(led_power = 150, sample_rate=400)

while (True):
    sensor.check()
    if (sensor.available()):
        red_sample = sensor.pop_red_from_storage()
        ir_sample = sensor.pop_ir_from_storage()
        print(f"[DATA] red={red_sample},ir={ir_sample},time={utime.ticks_ms()}")