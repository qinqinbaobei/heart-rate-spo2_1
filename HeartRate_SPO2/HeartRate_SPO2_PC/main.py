import threading
import dearpygui.dearpygui as dpg
import Max_30102
import serial
import winsound
import time
import json

# 采样数
Samplerate = 120

max_device = Max_30102.Device(sample_rate=Samplerate)
dpg.create_context()

with dpg.font_registry():
    default_font = dpg.add_font("simhei.ttf", 18)
    big_font = dpg.add_font("simhei.ttf", 100)

with dpg.theme() as global_theme:
    with dpg.theme_component(dpg.mvAll):
        dpg.add_theme_color(
            dpg.mvThemeCol_FrameBg, (0, 0, 0), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_color(
            dpg.mvThemeCol_WindowBg, (0, 0, 0), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_color(
            dpg.mvPlotCol_Line, (255, 255, 0), category=dpg.mvThemeCat_Plots
        )
        dpg.add_theme_color(
            dpg.mvPlotCol_FrameBg, (255, 255, 255), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_style(
            dpg.mvStyleVar_FrameRounding, 5, category=dpg.mvThemeCat_Core
        )


def MaxMinNormalization(x, Max, Min):
    if Max - Min > 0:
        x = 1 - (x - Min) / (Max - Min)
    else:
        x = 0
    return x


def update_data():
    while True:
        max_device.get_data()
        dpg.set_value("Raw", max_device.ir_list_data)
        # dpg.set_value("Raw", max_device.red_list_data_filtered)
        if max_device.data_valid:
            SPO2_now = MaxMinNormalization(
                max_device.red_list_data[-1],
                max(max_device.red_list_data),
                min(max_device.red_list_data),
            )
            SPO2_line_p = 370 + 233 * SPO2_now * -1
            dpg.configure_item("SPO2_line", p1=(830, SPO2_line_p))

            if max_device.filter_mode == "Filter OFF":
                dpg.configure_item("ViewMode", show=True)
                if max_device.view_mode == "Dynamic":
                    dpg.set_value(
                        "series_tag",
                        [
                            max_device.data_list_time,
                            max_device.red_list_data,
                        ],
                    )
                    dpg.set_axis_limits(
                        "x_axis",
                        min(max_device.data_list_time),
                        max(max_device.data_list_time),
                    )
                    dpg.set_axis_limits(
                        "y_axis",
                        min(max_device.red_list_data) - 5,
                        max(max_device.red_list_data) + 5,
                    )
                elif max_device.view_mode == "Scan":
                    dpg.set_value(
                        "series_tag",
                        [
                            max_device.range_list_time,
                            max_device.red_list_show_data,
                        ],
                    )
                    dpg.set_axis_limits(
                        "x_axis",
                        min(max_device.range_list_time),
                        max(max_device.range_list_time),
                    )
                    dpg.set_axis_limits(
                        "y_axis",
                        min(max_device.red_list_show_data) - 5,
                        max(max_device.red_list_show_data) + 5,
                    )
            else:
                dpg.configure_item("ViewMode", show=False)
                dpg.set_value(
                    "series_tag",
                    [
                        max_device.data_list_time,
                        max_device.red_list_data_filtered,
                    ],
                )
                dpg.set_axis_limits(
                    "x_axis",
                    min(max_device.data_list_time),
                    max(max_device.data_list_time),
                )
                dpg.set_axis_limits(
                    "y_axis",
                    min(max_device.red_list_data_filtered) - 5,
                    max(max_device.red_list_data_filtered) + 5,
                )

        dpg.set_value("bpm_text", "BPM: {:.0f} ".format(max_device.data["HR"]))
        dpg.set_value("SPO2_text", "SPO2: {:.1f} %".format(max_device.data["SPO2"]))


def port_choose():
    max_device.valid = False
    max_device.ser.close()
    max_device.port = dpg.get_value("port_list")
    max_device.ser = serial.Serial(max_device.port, max_device.brate)
    max_device.valid = True


def switch_view_mode():
    max_device.view_mode = dpg.get_value("ViewMode")

def switch_filter_mode():
    max_device.filter_mode = dpg.get_value("FilterMode")


with dpg.window(tag="primary", label="IR Graph") as win_prima:
    dpg.add_simple_plot(
        label="", tag="Raw", indent=25, default_value=(0, 0), height=100, width=800
    )
    SPO2_line = dpg.draw_line(
        (830, 370), (830, 370), tag="SPO2_line", color=(255, 255, 0, 255), thickness=12
    )
    with dpg.plot(
            label="SPO2", tag="SPO2_plot", height=300, width=820, crosshairs=True
    ):
        # optionally create legend
        dpg.add_plot_legend()

        # REQUIRED: create x and y axes
        dpg.add_plot_axis(dpg.mvXAxis, tag="x_axis")
        dpg.add_plot_axis(dpg.mvYAxis, tag="y_axis", invert=True, no_tick_labels=True)

        # series belong to a y axis
        dpg.add_line_series(
            max_device.data_list_time,
            max_device.red_list_data_filtered,
            parent="y_axis",
            tag="series_tag",
        )
    with dpg.drawlist(width=850, height=100):
        dpg.draw_rectangle(
            (0, 0),
            (850, 100),
            tag="beep_view",
            color=(255, 0, 0, 255),
            fill=(255, 0, 0, 255),
            show=False,
        )
    BPM_text = dpg.add_text("BPM:", pos=(0, 413), indent=20, tag="bpm_text")
    SPO2_text = dpg.add_text("SPO2:", indent=20, tag="SPO2_text")
    dpg.add_radio_button(
        ["Filter OFF", "Filter ON"],
        tag="FilterMode",
        default_value="Filter OFF",
        callback=switch_filter_mode,
        horizontal=True,
    )
    dpg.add_radio_button(
        ["Dynamic", "Scan"],
        tag="ViewMode",
        default_value="Dynamic",
        callback=switch_view_mode,
        horizontal=True,
    )
    port_list_combo = dpg.add_combo(
        items=max_device.port_list,
        width=200,
        indent=20,
        callback=port_choose,
        tag="port_list",
    )
    dpg.set_value("port_list", max_device.port)
    # dpg.add_text("IR:", tag="IR_text")
    # dpg.add_text("RED:", tag="RED_text")
    dpg.bind_font(default_font)
    dpg.bind_item_font(BPM_text, big_font)
    dpg.bind_item_font(SPO2_text, big_font)


def HR_live():
    while True:
        if max_device.data_valid:
            while max_device.k < Samplerate:
                time.sleep(0.01)
            while max_device.k >= Samplerate:
                time.sleep(0.01)
            max_device.hr_time_start_end.append(max_device.data["Time"])
            max_device.hr_time_start_end.pop(0)
            winsound.Beep(2000, 150)
            dpg.configure_item("beep_view", show=True)
            time.sleep(0.1)
            dpg.configure_item("beep_view", show=False)
        time.sleep(0.01)


def server_run():
    import socket

    ip_port = ("0.0.0.0", 9999)

    sk = socket.socket()  # 创建套接字
    sk.bind(ip_port)  # 绑定服务地址
    sk.listen(1)  # 监听连接请求
    print("等待客户端连接")
    while True:
        conn, address = sk.accept()  # 等待连接，此处自动阻塞
        print("客户端已连接", address)
        while True:
            try:
                conn.sendall(json.dumps(max_device.data).encode())
                time.sleep(0.05)
            except:
                conn.close()  # 关闭连接
                print("客户端已断开")
                break


threading.Thread(target=update_data, daemon=True).start()
threading.Thread(target=HR_live, daemon=True).start()
threading.Thread(target=server_run, daemon=True).start()
dpg.bind_theme(global_theme)
dpg.create_viewport(title="Heart Rate And SPO2", width=885, height=800)
dpg.set_primary_window("primary", True)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()
