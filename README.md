# HeartRateSPO2

#### 介绍
生命体征监测仪，脉搏心率，血氧饱和度，血氧波形，使用MAX30102模块

开发环境支持：Arduino IDE，MicroPython

硬件支持：Raspberrypi Pico，Arduino Nano/Uno，ESP32，STM32

![输入图片说明](image/MAX30102.png)

#### 软件架构
下位机硬件部分：Arduino IDE开发或者MicroPython开发支持硬件：Raspberrypi Pico，Arduino Nano/Uno，ESP32，STM32

上位机软件部分：Python 使用DearpyGui框架绘制桌面客户端

#### 运行教程
1. 单片机烧录程序
    - 使用ArduinoIDE烧录（HeartRate_SPO2_arduino/HeartRate_SPO2_arduino.ino/HeartRate_SPO2_arduino.ino）需要安装SparkFun_MAX3010x_Sensor_Library库。
    - 使用MicroPython（HeartRate_SPO2_micropython/*）注：该目录所有文件都需要上传到板子里。
2. PC安装Python依赖库，在（HeartRate_SPO2/HeartRate_SPO2_PC）目录 `pip install -r requirements.txt`
3. 运行上位机程序代码（HeartRate_SPO2/HeartRate_SPO2_PC/main.py），会自动识别可用串口，若串口设备较多识别的串口不正确，在窗口下方可以切换
4. PC程序有socket服务，有连接时不间断发送数据，端口：9999，连接示例：（HeartRate_SPO2/HeartRate_SPO2_PC/client.py）

#### 使用说明
1.  MAX30102连接电源，通过IIC连接单片机

- Raspberrypi Pico 电路连接，使用Arduino IDE：IO4->SDA，IO5->SCL

![Raspberrypi_Pico](image/Raspberrypi_Pico.png)

- Arduino Nano、Uno 电路连接，使用Arduino IDE：A4->SDA，A5->SCL，

![Arduino_Nano](image/Arduino_Nano.png)

- ESP32 电路连接：使用MicroPython：IO10->SDA，IO6->SCL，使用ArduinoIDE：IO8->SDA，IO9->SCL

![ESP32_C3](image/ESP32_C3.png)

- STM32 电路连接，使用Arduino IDE：B7->SDA，B6->SCL

![STM32F103](image/STM32F103.png)

#### 上位机数据格式
如果您的硬件在这里不支持（理论上可以用Arduino IDE编程的硬件都支持），或者自行开发，想适配本项目的上位机，可以用以下数据格式与上位机通信。
> [DATA] red=XXX,ir=XXX,time=XXX

例如
> [DATA] red=1000,ir=1000,time=1000

注：time的单位为ms

#### 硬件程序兼容对应表
| 硬件               | Arduino IDE 开发 | MicroPython 开发 |
|------------------|-----------|---------------|
| Raspberrypi Pico | 支持        | 支持            |
| Arduino Nano/Uno | 支持        | 不支持           |
| ESP32            | 支持        | 支持            |
| STM32            | 支持        | 支持            |

#### 开发注意事项 :tw-1f61c: 
1. 使用Arduino IDE除了Nano/Uno外，Raspberrypi Pico、ESP32和STM32都需要安装对应的环境。
2. 使用MicroPython需要预先烧录MicroPython固件。
3. 使用Arduino IDE开发和使用MicroPython所使用的可能引脚不同，按需要修改。
4. 环境安装方法百度有很多，实在装不上的话可以加个微信相互学习 :tw-1f61c: 。

#### 链接列表
| 网站 | 网址 | 备注 |
|----|----|----|
| MicroPython官网 | https://micropython.org/download/ | 下载MicroPython固件 |
| Arduino官网 | https://www.arduino.cc/en/software | 下载Arduino IDE开发环境 |
| Arduino_STM32项目 | https://github.com/rogerclarkmelbourne/Arduino_STM32 | 下载STM32的Arduino开发环境 |
| ESP32 With Arduino官方开发文档 | https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html#installing-using-arduino-ide | 参考安装ESP32的Arduino IDE环境 |

#### 使用注意事项 :tw-1f61c: 
1. MAX30102在手指按压时易触碰到模块上的外围电路引起干扰和短路，使用时需要将外围电路隔离，例如：贴胶带。
2. MAX30102是反射式传感器，精度相较于透射式传感器略低，也会因手指按压力度影响测量结果，结果仅供参考，不能作为临床诊断依据。
3. 如心率不准确，微调滤波器参数。

#### 演示
![动图](image/%E6%BC%94%E7%A4%BA.gif)
![](image/%E6%88%AA%E5%9B%BE1.png)
![](image/%E6%88%AA%E5%9B%BE2.png)

#### 关于算法
心率算法：自动多尺度峰值查找算法（Ampd）

#### 开发进程
持续更新算法中

#### 更新记录
- 2023-1-12 更新了显示模式，新增扫描显示
- 2023-1-13 新增STM32

#### 相互学习
![微信](image/Wechat_Chenxi.png)
